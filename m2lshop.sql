-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 29 mars 2023 à 20:47
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `m2lshop`
--

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Structure de la table `categorieproduit`
--

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `reference` varchar(100) NOT NULL,
  `nomProduit` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `prix` float DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  PRIMARY KEY (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--
INSERT INTO `produit` (`reference`, `nomProduit`, `description`, `prix`, `quantite`) VALUES
('248220', 'Step', 'Pour vous sentir bien dans votre peau.', 34.99, 7),
('248246', 'Tapis de yoga', 'Pour vous sentir bien dans votre peau.', 19, 12),
('2525', 'Balle de golf', 'Vise le trou, avec le moins de coups.', 2, 159),
('253214', 'Haltère', 'Pour vous muscler, les haltères vous aiderons.', 53.5, 4),
('27272824', 'Balle de foot', 'Une balle digne des plus performante, avec celle-ci, la lucarne sera votre confidente.', 20, 7),
('272782', 'Balle de volley', 'Pour vous amusez à la plage, ou chez vous.', 24, 8),
('292852', 'Gants de boxe', 'Pour combattre dans un ring...', 29.99, 7);
-- --------------------------------------------------------

--
-- Structure de la table `produitcommande`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomUtilisateur` varchar(100) DEFAULT NULL,
  `mdp` varchar(100) DEFAULT NULL,
  `dateCreation` date DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `prenomUtilisateur` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--
INSERT INTO `utilisateur` (`id`, `nomUtilisateur`, `mdp`, `dateCreation`, `role`, `mail`, `prenomUtilisateur`) VALUES
(1, 'Albiach', '$2a$08$NSYe3jbrpDuqvhK0YkfLlO.NJG0jFfHrzzoFcileSwTsvdeY5dfAu', '2023-03-29', 'admin', 'l.albiach@ecole-ipssi.net', 'Loan');
--
-- Contraintes pour les tables déchargées
--



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
