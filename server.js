const mariadb = require("mariadb");
const express = require("express");
const app = express();
var cors = require("cors");
var bcrypt = require("bcryptjs");
module.exports = app;

require("dotenv").config();

//Connexion vers la base de donnée
const pool = mariadb.createPool({
  host: "127.0.0.1",
  database: "m2lshop",
  user: "root",
  password: "",
});

// Utilisation du module express 
app.use(express.json());
app.use(cors());

// Récupérer la liste des produits
app.get("/produits", async (req, res) => {
  let conn;
  conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM produit;");
  res.status(200).json(rows);
  conn.release();
});

// Récupérer la liste des produits ayant une quantité inférieur à 10
app.get("/produits/inferieurProduits", async (req, res) => {
  let conn;
  conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM produit WHERE quantite < 10;");
  res.status(200).json(rows);
  conn.release();
});

// Récupérer la liste des produits ayant une quantité inférieur à 10
app.get("/produits/neverBuy", async (req, res) => {
  let conn;
  conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM produit WHERE produit.reference NOT IN (SELECT idProduit FROM achete);");
  res.status(200).json(rows);
  conn.release();
});

// Récupérer la liste des produits trié par prix
app.get("/produits/prix", async (req, res) => {
  let conn;
  conn = await pool.getConnection();
  const rows = await conn.query(`SELECT * FROM produit ORDER BY prix ASC;`);
  res.status(200).json(rows);
  conn.release();
});

// Récupérer la liste des produits trié par nom
app.get("/produits/nomProduit", async (req, res) => {
  let conn;
  conn = await pool.getConnection();
  const rows = await conn.query(`SELECT * FROM produit ORDER BY nomProduit ASC;`);
  res.status(200).json(rows);
  conn.release();
});

// Récupérer un produit avec sa référence
app.get("/produit/:ref", async (req, res) => {
  let conn;
  let ref = req.params.ref;
  conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM produit WHERE reference = ?;", [
    ref,
  ]);
  res.status(200).json(rows);
  conn.release();
});

// Ajouter un produit 
app.post("/produit", async (req, res) => {
  let conn;
  try {
    conn = await pool.getConnection();
    await conn.query(
      "INSERT INTO produit(nomProduit, description, prix, quantite) VALUES (?,?,?,?);",
      [
        req.body.nomProduit,
        req.body.description,
        req.body.prix,
        req.body.quantite
      ]
    );
    res.status(200).send("Produit ajouté avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur lors de l'ajout d'un produit");
    conn.release();
  }
});

//Ajouter une commande dans la table achete
app.post("/cart", async (req, res) => {
  let conn;
  try {
    conn = await pool.getConnection();
    await conn.query(
      "INSERT INTO achete(idUtilisateur, idProduit, quantite) VALUES (?,?,?);",
      [
        req.body.idUtilisateur,
        req.body.idProduit,
        req.body.quantite,
      ]
    );
    res.status(200).send("Produit du panier acheté");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur lors de l'achat du produit");
    conn.release();
  }
});


// Modifier un produit avec sa référence
app.put("/produit/:ref", async (req, res) => {
  try {
    let conn;
    let ref = req.params.ref;
    conn = await pool.getConnection();
    await conn.query(
      "UPDATE produit SET nomProduit = ?, description = ?, prix = ?, quantite = ? WHERE reference = ?",
      [
        req.body.nomProduit,
        req.body.description,
        req.body.prix,
        req.body.quantite,
        ref,
      ]
    );
    res.status(200).send("Produit mis à jour avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Une erreur est survenue lors de la mise à jour");
    if (conn) conn.release();
  }
});


// Suppression d'un produit avec sa référence
app.delete("/produit/:ref", async (req, res) => {
  try {
    let conn;
    let ref = req.params.ref;
    conn = await pool.getConnection();
    await conn.query(`DELETE FROM produit WHERE reference = ?;`, [ref]);
    res.status(200).send("Produit supprimé avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).json({
      message: "Erreur de suppression du produit"
    });
    if (conn) conn.release();
  }
});

// Voir la liste de tous les utilisateurs
app.get("/user", async (req, res) => {
  let conn;
  try {
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM utilisateur;");
    res.status(200).json(rows);
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur de la récupération de la liste des utilisateurs");
  }
  if (conn) conn.release();
});


// Créer un nouvelle utilisateur
app.post("/user/new", async (req, res) => {
  var hash = bcrypt.hashSync(req.body.mdp, 8);
  try {
    let conn;
    conn = await pool.getConnection();
    await conn.query(
      "INSERT INTO utilisateur(nomUtilisateur, mdp, dateCreation, role, mail, prenomUtilisateur) VALUES (?,?,NOW(),?,?,?)",
      [
        req.body.nomUtilisateur,
        hash,
        req.body.role,
        req.body.mail,
        req.body.prenomUtilisateur,
      ]
    );
    res.status(200).send("Ajout avec succès d'un utilisateur");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur lors de l'ajout d'un utilisateur");
  }
});


// Se connecter à son compte
app.post("/login", async (req, res) => {
  try {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM utilisateur WHERE mail = ?;", [
      req.body.mail,
    ]);
    if (rows.length == 0) {
      // utilisateur n'existe pas
      res.status(400).send("Email ou mot de passe incorrect.");
    } else {
      // vérification du mot de passe

      const hash = rows[0].mdp;
      const isValidPassword = await bcrypt.compare(req.body.mdp, hash);
      if (!isValidPassword) {
        res.status(400).send("Email ou mot de passe incorrect.");
        conn.release();
      } else {
        // connexion réussie
        res.status(200).json(rows[0]);
        conn.release();
      }
    }

  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur de connexion au compte");

  }
});
// Seg connecter à son compte
app.post("/loginA", async (req, res) => {
  try {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM utilisateur WHERE mail = ? AND role = ?", [
      req.body.mail,
      'admin',
    ]);
    if (rows.length == 0) {
      // utilisateur n'existe pas
      res.status(400).send("Email ou mot de passe incorrect.");
    } else {
      // vérification du mot de passe
      const hash = rows[0].mdp;
      const isValidPassword = await bcrypt.compare(req.body.mdp, hash);
      if (!isValidPassword) {
        res.status(400).send("Email ou mot de passe incorrect.");
      } else {
        // connexion réussie
        res.status(200).json(rows[0]);
      }
    }
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur de connexion au compte");
    conn.release();
  }
});

//Modifier un utilisateur avec son id
app.put("/user/:id", async (req, res) => {
  try {
    let conn;
    let id = req.params.id;
    conn = await pool.getConnection();
    await conn.query(
      `UPDATE utilisateur SET nomUtilisateur = ?, prenomUtilisateur = ?, mail = ?,  role = ? WHERE id = ${id};`,
      [req.body.nomUtilisateur, req.body.prenomUtilisateur, req.body.mail, req.body.role]
    );
    res.status(200).send("Modification de l'utilisateur avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur lors de la modification de l'utilistateur");
    conn.release();
  }
});




// Supprimer un utilisateur avec son id
app.delete("/user/:ref", async (req, res) => {
  try {
    let conn;
    let id = req.params.ref;
    conn = await pool.getConnection();
    await conn.query(`DELETE FROM utilisateur WHERE id = ?;`, [id]);
    res.status(200).send("Suppression de l'utilisateur avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur de suppression de l'utilisateur");
    conn.release();
  }
});

// Serveur à l'écoute
app.listen(8000, () => {
  console.log("Serveur à l'écoute");
});